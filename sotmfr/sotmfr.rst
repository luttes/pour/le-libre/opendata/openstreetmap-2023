.. index::
   pair: State of  the map ; 2023-06-09

.. #sotmus #sotm #StateOfTheMap #StateOfTheMapUS #OpenStreetMap #OSM @OpenStreetMapUS

.. _sotmfr_2023:

=============================================================================================================================
|sotm| **State of the map France 2023**
=============================================================================================================================

- https://sotm2023.openstreetmap.fr/
- https://sotm2023.openstreetmap.fr/infos.html
- https://fr.osm.social/@osm_fr
- https://mapstodon.space/@panoramax

#sotm2023 #sotmfr2023 #SotmFR #osmfr #sotm #sotm #sotmfr2023 #sotmfr#Marseille

Après Lyon (2013), Paris (2014), Brest (2015), Clermont-Ferrand (2016),
Avignon (2017), Bordeaux (2018), Montpellier (2019) et Nantes (2022)
c'est au tour de Marseille d'accueillir les 9èmes Rencontres Nationales
OpenStreetMap : le State Of The Map France (SotM-fr).

Les 9-10-11 juin 2023, durant 3 jours, contributeurs, utilisateurs,
représentants de collectivités et d'entreprises gravitant autour du
Web et de l'information géographique, chercheurs, mais aussi personnes
curieuses de découvrir cette « carte libre du monde » que représente
OSM, se retrouveront pour partager leurs expériences, se tenir informé,
se former, découvrir l'écosystème et les multiples applications – existantes
ou à imaginer – autour d'OpenStreetMap.




.. toctree::
   :maxdepth: 5

   06/06

