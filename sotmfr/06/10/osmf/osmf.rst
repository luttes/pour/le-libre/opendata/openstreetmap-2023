.. index::
   pair: Sarah Hoffmann ; La fondation openstreetmap
   ! Sarah Hoffmann


.. _sotmfr_2023_09_06:

======================================================================
2023-06-10 **La fondation openstreetmap**  |osm| par Sarah Hoffmann
======================================================================

- https://fr.wikipedia.org/wiki/Fondation_OpenStreetMap
- https://wiki.osmfoundation.org/wiki/About
- https://blog.openstreetmap.org/2023/01/19/get-to-know-the-new-osmf-board/
- https://peertube.openstreetmap.fr/w/oLtgX883cm2dasBUZKrfg8

.. _sara_hoffmann:

Sarah Hoffmann
==================

- https://github.com/lonvia
- https://blog.openstreetmap.org/2023/01/19/get-to-know-the-new-osmf-board/
- https://wiki.openstreetmap.org/wiki/User:Lonvia

.. figure:: images/sarah_hoffmann.png
   :align: center

   https://wiki.openstreetmap.org/wiki/User:Lonvia


Sarah Hoffmann has been contributing to OpenStreetMap since 2008 under
the username of lonvia (she also contributes to the wiki under the same name).

She started out as a simple mapper, collecting a lot of data while hiking
in the Swiss Alps.

Over the years she became more and more involved in software development
for OSM.

She is maintainer for:

- `Nominatim <https://github.com/osm-search/Nominatim>`_ (https://github.com/osm-search/Nominatim)
- `osm2pgsql <https://github.com/openstreetmap/osm2pgsql>`_ ,
  (https://github.com/openstreetmap/osm2pgsql)
- `pyosmium <https://github.com/osmcode/pyosmium>`_ (https://github.com/osmcode/pyosmium)
- `waymarkedtrails.org <https://github.com/waymarkedtrails/waymarked-trails-site>`_
  (https://github.com/waymarkedtrails/waymarked-trails-site)
- and a `couple of other projects <https://github.com/lonvia>`_.

She maintains a couple of route maps for different purposes:

- `hiking <http://hiking.waymarkedtrails.org/>`_ (http://hiking.waymarkedtrails.org/)
- `cycling <http://cycling.waymarkedtrails.org/>`_ (http://cycling.waymarkedtrails.org/)
- `mountain-biking <http://mtb.waymarkedtrails.org/>`_ (http://mtb.waymarkedtrails.org/)
- `inline skating <http://skating.waymarkedtrails.org/>`_ (http://skating.waymarkedtrails.org)

She is part of the OSMF sysadmin team where she is responsible for the
`Nominatim servers <https://github.com/osm-search/Nominatim>`_ and has
helped out in the programme committee of `State of the Map <https://wiki.openstreetmap.org/wiki/State_of_the_Map>`_
(https://wiki.openstreetmap.org/wiki/State_of_the_Map) over the last
couple of years. |sotm|

In 2020 she finally gave up pretending that OSM is just a hobby.

Nowadays she works as a freelancer doing development and consulting for
OSM software in general and Nominatim in particular.

She lives in Dresden, Germany.

La fondation openstreetmap  |osm|
====================================

.. figure:: images/la_fondation_openstreetmap.png
   :align: center

Mission de base
===================

.. figure:: images/mission_de_base.png
   :align: center

Campagne de dons
===============================

.. figure:: images/campagne_de_dons.png
   :align: center


Contribuer comme entreprise
===============================

.. figure:: images/contribuer_comme_entreprise.png
   :align: center


Contribuer comme individu
===============================

.. figure:: images/contribuer_comme_individu.png
   :align: center
