.. index::
   pair: panoramax ; 2023-06-09

.. _panoramax_2023_06_09:

==============================================================================================================
|panoramax| 2023-06-09 **panoramax** (L’alternative libre pour photo-cartographier les territoires)
==============================================================================================================

- https://panoramax.fr/
- https://wiki.openstreetmap.org/wiki/FR:Panoramax.openstreetmap.fr
- https://forum.geocommuns.fr/


Choix de la licence CC-BY-SA 4.0 + autorisation à utiliser des données dérivées
=====================================================================================


.. figure:: images/choix_licence.png
   :align: center


Vitrine du projet
========================

.. figure:: images/vitrine_panoramax.png
   :align: center



Histoire panoramax
========================

.. figure:: images/histoire_panoramax.png
   :align: center


Communauté
=============

- https://wiki.openstreetmap.org/wiki/FR:Panoramax.openstreetmap.fr
- https://forum.geocommuns.fr/


.. figure:: images/panoramax.png
   :align: center

Le public
============

.. figure:: images/public.png
   :align: center

