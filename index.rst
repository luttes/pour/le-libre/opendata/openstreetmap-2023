.. index::
   ! Openstreetmap 2023

.. raw:: html

    <a rel="me" href="https://qoto.org/@pvergain"></a>
    <a rel="me" href="https://framapiag.org/@pvergain"></a>


.. _openstreetmap_2023:

===========================================
|osm| **Openstreetmap 2023**
===========================================


.. toctree::
   :maxdepth: 5

   news/news
   sotmfr/sotmfr

