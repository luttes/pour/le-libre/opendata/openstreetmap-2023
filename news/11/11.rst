.. index::
   pair: OpenStreetMap; IUGA

============================================================================================
2023-11
============================================================================================



2023-11-14 **La Fédération des pros d’OSM, OpenStreetMap, avec Florian Lainez, président, et Jean-Christophe Becquet, vice-président de l’April et de la Fédération**
=============================================================================================================================================================================

- https://www.librealire.org/emission-libre-a-vous-diffusee-mardi-14-novembre-2023-sur-radio-cause-commune
- https://www.librealire.org/emission-libre-a-vous-diffusee-mardi-14-novembre-2023-sur-radio-cause-commune#s-La-Federation-des-pros-d-OSM-OpenStreetMap-avec-Florian-Lainez-nbsp

La Fédération des pros d’OSM, OpenStreetMap, avec Florian Lainez, président,
et Jean-Christophe Becquet, vice-président de l’April et de la Fédération

Frédéric Couchet : Nous allons poursuivre par notre sujet principal qui
porte sur la Fédération des pros d’OSM, OpenStreetMap, le Wikipédia de
la cartographie, fédération qui a été officiellement lancée il y a un an,
en novembre 2022, mais qui a peut-être été créée quelques mois avant.

Nos invités du jour, Florian Lainez qui est avec moi en studio.
Bonjour Florian.

Florian Lainez : Bonjour.

Frédéric Couchet : Et normalement, nous avons Jean-Christophe Becquet
à distance. Bonjour Jean-Christophe.

Jean-Christophe Becquet : Bonjour à tous. Bonjour à toutes.

Frédéric Couchet : Avant de commencer, de vous laisser la parole de
présentation, je voulais quand même préciser qu’initialement c’était
Cécile Guégan, de la Fédération, qui devait intervenir, mais elle ne
peut pas, donc merci à Jean-Christophe d’avoir pris le temps de la
remplacer aujourd’hui.

N’hésitez pas à participer à notre conversation, soit par téléphone
au 09 72 51 55 46 ou sur le salon web dédié à l’émission, sur le site
causecommune.fm, bouton de « chat » ou, nouveauté, expérimentation,
vous pouvez aussi utiliser le hashtag #libreavous sur Mastodon ou nous
envoyer un message sur notre compte mastodon aprilorg.
